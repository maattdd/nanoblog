require 'erubis'
require 'kramdown'
require 'rugged'
require 'time'
require 'stringex'

class Nanoblog
  def initialize(folder = '.')
    @folder = folder
    @repo = Rugged::Repository.new(@folder)
    @walker = Rugged::Walker.new(@repo)
    @walker.sorting(Rugged::SORT_DATE)
  rescue StandardError
    puts 'Invalid folder'
    exit
  end

  def get_posts
    posts = []
    @repo.head.target.tree.each_blob do |file|
      file[:name][-3..-1] == '.md' || next
      text = @repo.lookup(file[:oid]).content
      title = file[:name].tr('_', ' ')[0..-4]
      content = Kramdown::Document.new(text, input: 'GFM', syntax_highlighter: nil)
      posts << {
        title:    title,
        author:   get_author_of_post(file[:name])[:name],
        content:  content.to_html,
        slug:     title.to_url
      }.merge(get_date_of_post(file[:name]))
    end

    posts.sort_by { |post| post[:created_at] }.reverse
  end

  def get_template
    @repo.head.target.tree.each_blob do |file|
      file[:name] == 'template.erb' && (return @repo.lookup(file[:oid]).content)
    end
    # fallback template
    File.read(__dir__ + '/template.erb')
  end

  def get_commits(filename)
    tab = []
    @walker.push(@repo.head.target)
    @walker.each do |commit|
      tab.push(commit) if commit.diff(paths: [filename]).size > 0
    end
    tab
  end

  def get_date_of_post(filename)
    commits = get_commits(filename)
    {
      created_at: commits.last.time.iso8601,
      updated_at: commits.size > 1 ? commits.first.time.iso8601 : nil
    }.compact
  end

  def get_author_of_post(filename)
    get_commits(filename).last.author
  end

  def posts?
    !get_posts.empty?
  end

  def run
    eruby = Erubis::Eruby.new(get_template)
    context = Erubis::Context.new(posts: get_posts, title: File.basename(File.expand_path(@folder)))
    eruby.evaluate(context)
  end
end
