Gem::Specification.new do |s|
  s.name        = 'nanoblog'
  s.version     = '0.0.2'
  s.date        = '2017-12-28'
  s.summary     = 'Nano blog generator'
  s.description = 'a light blog generator from markdown'
  s.authors     = ['Matthieu Dubet']
  s.email       = 'matthieu.dubet@gmail.com'
  s.files       = ['lib/nanoblog.rb', 'lib/template.erb']
  s.executables = ['nanoblog']
  s.homepage    = 'http://rubygems.org/gems/nanoblog'
  s.license     = 'MIT'

  s.add_dependency 'erubis', '~> 2.0'
  s.add_dependency 'kramdown', '~> 1.0'
  s.add_dependency 'rugged', '~> 0.26'
  s.add_dependency 'stringex', '~> 2.0'
end
